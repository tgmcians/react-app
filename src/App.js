import React from "react";
import ExcelReader from "./components/ExcelReader";

function App() {
  return <ExcelReader />;
}

export default App;
