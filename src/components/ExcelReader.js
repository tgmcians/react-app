import React, { PureComponent } from "react";
import XLSX from "xlsx";
import ExcelViewer from "./ExcelViewer";

export default class ExcelReader extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      file: {},
      data: []
    };
  }

  handleChange = event => {
    const files = event.target.files;
    if (files && files[0]) this.setState({ file: files[0] });
  };

  handleSubmit = () => {
    const reader = new FileReader();
    const rABS = !!reader.readAsBinaryString;

    reader.onload = e => {
      /* Parse data */
      const bstr = e.target.result;
      const wb = XLSX.read(bstr, {
        type: rABS ? "binary" : "array",
        bookVBA: true
      });
      /* Get first worksheet */
      const wsname = wb.SheetNames[0];
      const ws = wb.Sheets[wsname];
      /* Convert array of arrays */
      const data = XLSX.utils.sheet_to_json(ws);
      console.log(data);
      this.setState({ data });
    };

    reader.readAsBinaryString(this.state.file);
  };

  render() {
    const { data } = this.state;
    return (
      <div>
        <input
          type="file"
          id="fileSelect"
          accept=".xlsx, .xls"
          onChange={this.handleChange}
        />
        <input type="submit" value="Submit" onClick={this.handleSubmit} />
        <ExcelViewer data={data} />
      </div>
    );
  }
}
