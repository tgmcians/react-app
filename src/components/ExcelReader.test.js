import React from "react";
import { shallow, configure } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import ExcelReader from "./ExcelReader";

configure({ adapter: new Adapter() });

describe("Excel Reader component", () => {
  it("renders with ID and ABC value", () => {
    const component = shallow(<ExcelReader />);
    expect(component).toMatchSnapshot();
  });

  it("simulate handle change of file input", () => {
    const component = shallow(<ExcelReader />);
    component
      .find("input")
      .at(0)
      .simulate("change", {
        target: {
          files: ["dummyValue.something"]
        }
      });

    expect(component.state("file")).toEqual("dummyValue.something");
  });
});
