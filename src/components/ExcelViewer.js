import React from "react";

const ExcelViewer = props => {
  const { data } = props;
  if (data.length > 0)
    return (
      <div>
        <table>
          <tbody>
            {data &&
              data.map(item => {
                if (item.ABC) {
                  return (
                    <tr>
                      <td>{item.Name}</td>
                      <td>{item.ABC}</td>
                    </tr>
                  );
                } else if (
                  item.__EMPTY_1 === "sub2" ||
                  item.__EMPTY_1 === undefined
                ) {
                  return (
                    <tr>
                      <td>{item.Name}</td>
                      <td>{item.__EMPTY_2}</td>
                    </tr>
                  );
                } else {
                  return null;
                }
              })}
          </tbody>
        </table>
      </div>
    );
  return null;
};

export default ExcelViewer;
