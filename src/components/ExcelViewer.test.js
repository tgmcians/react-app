import React from "react";
import { shallow, configure } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import ExcelViewer from "./ExcelViewer";

configure({ adapter: new Adapter() });

describe("Excel Viewer component", () => {
  it("renders with ID and ABC value", () => {
    const props = {
      data: [
        { Name: "ID", ABC: "13545", __rowNum__: 3 },
        {
          Name: "Registration date",
          ABC: "Tuesday, September 04, 1990",
          __rowNum__: 4
        }
      ]
    };

    const component = shallow(<ExcelViewer {...props} />);
    expect(component).toMatchSnapshot();
  });

  it("render with level with multiple sub", () => {
    const props = {
      data: [{ __EMPTY_1: "sub2", Name: "a2", __EMPTY_2: "2", __rowNum__: 9 }]
    };
    const component = shallow(<ExcelViewer {...props} />);
    expect(component).toMatchSnapshot();
  });

  it("render with level with single sub", () => {
    const props = {
      data: [{ Name: "b2", __EMPTY_2: "90", __rowNum__: 11 }]
    };
    const component = shallow(<ExcelViewer {...props} />);
    expect(component).toMatchSnapshot();
  });

  it("render with no data", () => {
    const props = {
      data: []
    };
    const component = shallow(<ExcelViewer {...props} />);
    expect(component).toMatchSnapshot();
  });
});
